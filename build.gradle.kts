import java.util.Properties

val projectSettings = fetchProjectSettings()

group = "io.gitlab.embed-soft"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.5.31"
    `maven-publish`
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    explicitApi()
    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            cinterops.create("efl") {
                val baseIncludeDir = "/mnt/pi_image/usr/local/include"
                includeDirs(
                    "$baseIncludeDir/ecore-1",
                    "$baseIncludeDir/ecore-con-1",
                    "$baseIncludeDir/eina-1",
                    "$baseIncludeDir/eina-1/eina",
                    "$baseIncludeDir/eo-1",
                    "$baseIncludeDir/efl-1",
                    "$baseIncludeDir/elementary-1",
                    "$baseIncludeDir/evas-1",
                    "$baseIncludeDir/edje-1"
                )
            }
            dependencies {
                implementation("io.gitlab.embed-soft:eflkt-core:${projectSettings.libVer}")
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.5.31"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("io.gitlab.embed-soft:eflkt-core:${projectSettings.libVer}")
            }
        }
    }
}

data class ProjectSettings(val depLibVer: String, val libVer: String, val isDevVer: Boolean, val includeDocs: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    var depLibVer = ""
    var includeDocs = false
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        depLibVer = properties.getProperty("depLibVer") ?: ""
        @Suppress("RemoveSingleExpressionStringTemplate")
        isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
        @Suppress("RemoveSingleExpressionStringTemplate")
        includeDocs = "${properties.getProperty("includeDocs")}".toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer, depLibVer = depLibVer, includeDocs = includeDocs)
}

fun MavenPublication.createPom() = pom {
    name.set("EFL KT UI")
    description.set("A Kotlin Native library that provides GUI functionality in a Kotlin Native project.")
    url.set("https://gitlab.com/embed-soft/efl-kt/efl-kt-ui")

    licenses {
        license {
            name.set("Apache 2.0")
            url.set("https://opensource.org/licenses/Apache-2.0")
        }
    }
    developers {
        developer {
            id.set("NickApperley")
            name.set("Nick Apperley")
            email.set("napperley@protonmail.com")
        }
    }
    scm {
        url.set("https://gitlab.com/embed-soft/efl-kt/efl-kt-ui")
    }
}
