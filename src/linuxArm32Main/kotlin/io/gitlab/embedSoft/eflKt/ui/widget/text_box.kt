package io.gitlab.embedSoft.eflKt.ui.widget

import efl.EFL_UI_TEXTBOX_CLASS
import efl.Eo
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.AutoRepeat
import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.input.Clickable
import io.gitlab.embedSoft.eflKt.ui.text.DisplayableText
import kotlinx.cinterop.CPointer

public actual class TextBox private constructor(
    ptr: CPointer<Eo>?
) : WidgetBase, DisplayableText, Content, AutoRepeat, Clickable {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual companion object {
        public actual fun create(parent: EflObjectBase): TextBox =
            TextBox(EflObject.create(classType = EFL_UI_TEXTBOX_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): TextBox =
            TextBox(EflObject.createWithReference(classType = EFL_UI_TEXTBOX_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun textBox(parent: EflObjectBase?, withRef: Boolean, init: TextBox.() -> Unit): TextBox {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val txtBox = if (!withRef && parent != null) TextBox.create(parent) else TextBox.createWithReference(parent)
    txtBox.init()
    return txtBox
}
