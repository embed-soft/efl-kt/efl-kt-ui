package io.gitlab.embedSoft.eflKt.ui.text

import efl.efl_text_markup_get
import efl.efl_text_markup_set
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.toKString

public actual var EflObjectBase.textMarkup: String
    get() = efl_text_markup_get(eoPtr)?.toKString() ?: ""
    set(value) = efl_text_markup_set(eoPtr, value)
