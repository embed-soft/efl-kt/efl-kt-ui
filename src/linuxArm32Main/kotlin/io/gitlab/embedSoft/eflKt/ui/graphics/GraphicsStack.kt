package io.gitlab.embedSoft.eflKt.ui.graphics

import efl.Efl_Gfx_Stack
import efl.Eo
import kotlinx.cinterop.CPointer

public actual class GraphicsStack private constructor(ptr: CPointer<Efl_Gfx_Stack>?): GraphicsStackBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<Efl_Gfx_Stack>?): GraphicsStack = GraphicsStack(ptr)
    }
}
