package io.gitlab.embedSoft.eflKt.ui

import efl.efl_ui_selectable_fallback_selection_get
import efl.efl_ui_selectable_fallback_selection_set
import efl.efl_ui_selectable_last_selected_get
import io.gitlab.embedSoft.eflKt.core.EflObjectBase

public actual interface SingleSelectable : EflObjectBase {
    public actual var fallbackSelection: Selectable?
        get() = efl_ui_selectable_fallback_selection_get(eoPtr)?.toSelectable()
        set(value) = efl_ui_selectable_fallback_selection_set(eoPtr, value?.eoPtr)

    public actual val lastSelected: Selectable
        get() = efl_ui_selectable_last_selected_get(eoPtr).toSelectable()
}
