package io.gitlab.embedSoft.eflKt.ui.widget

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.booleanValue
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.InteractiveRange
import kotlinx.cinterop.CPointer

public actual class SpinButton private constructor(ptr: CPointer<Eo>?): SpinBase, InteractiveRange {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual var wrapAround: Boolean
        get() = efl_ui_spin_button_wraparound_get(eoPtr).booleanValue
        set(value) = efl_ui_spin_button_wraparound_set(eoPtr, value.uByteValue)

    public actual companion object {
        public actual fun create(parent: EflObjectBase): SpinButton =
            SpinButton(EflObject.create(classType = EFL_UI_SPIN_BUTTON_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): SpinButton =
            SpinButton(EflObject.createWithReference(classType = EFL_UI_SPIN_BUTTON_CLASS, parent = parent,
                init = {}).eoPtr)
    }
}

public actual fun spinButton(parent: EflObjectBase?, withRef: Boolean, init: SpinButton.() -> Unit): SpinButton {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val spinBtn = if (!withRef && parent != null) SpinButton.create(parent) else SpinButton.createWithReference(parent)
    spinBtn.init()
    return spinBtn
}
