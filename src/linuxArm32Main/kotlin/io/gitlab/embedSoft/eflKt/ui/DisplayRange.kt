package io.gitlab.embedSoft.eflKt.ui

import efl.efl_ui_range_limits_get
import efl.efl_ui_range_limits_set
import efl.efl_ui_range_value_get
import efl.efl_ui_range_value_set
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.*

public actual interface DisplayRange : EflObjectBase {
    public actual var rangeLimits: Pair<Double, Double>
        get() = memScoped {
            val min = alloc<DoubleVar>()
            val max = alloc<DoubleVar>()
            efl_ui_range_limits_get(obj = eoPtr, min = min.ptr, max = max.ptr)
            min.value to max.value
        }
        set(value) = efl_ui_range_limits_set(obj = eoPtr, min = value.first, max = value.second)

    public actual var rangeValue: Double
        get() = efl_ui_range_value_get(eoPtr)
        set(value) = efl_ui_range_value_set(eoPtr, value)
}
