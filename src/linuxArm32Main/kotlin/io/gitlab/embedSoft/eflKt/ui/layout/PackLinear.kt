package io.gitlab.embedSoft.eflKt.ui.layout

import efl.*
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntity
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase

public actual interface PackLinear : Pack {
    public actual fun packAfter(subObj: GraphicsEntityBase, existing: GraphicsEntityBase?): Boolean = efl_pack_after(
        obj = eoPtr,
        subobj = subObj.eoPtr,
        existing = existing?.eoPtr
    ) == true.uByteValue

    public actual fun packAt(subObj: GraphicsEntityBase, index: Int): Boolean = efl_pack_at(
        obj = eoPtr,
        subobj = subObj.eoPtr,
        index = index
    ) == true.uByteValue

    public actual fun packBefore(subObj: GraphicsEntityBase, existing: GraphicsEntityBase?): Boolean = efl_pack_before(
        obj = eoPtr,
        existing = existing?.eoPtr,
        subobj = subObj.eoPtr
    ) == true.uByteValue

    public actual fun packBegin(subObj: GraphicsEntityBase): Boolean =
        efl_pack_begin(eoPtr, subObj.eoPtr) == true.uByteValue

    public actual fun getPackContent(index: Int): GraphicsEntityBase? {
        val ptr = efl_pack_content_get(eoPtr, index)
        return if (ptr != null) GraphicsEntity.fromPointer(ptr) else null
    }

    public actual fun packEnd(subObj: GraphicsEntityBase): Boolean =
        efl_pack_end(eoPtr, subObj.eoPtr) == true.uByteValue

    public actual fun getPackIndex(subObj: GraphicsEntityBase): Int = efl_pack_index_get(eoPtr, subObj.eoPtr)

    public actual fun unpackAt(index: Int): GraphicsEntityBase? {
        val ptr = efl_pack_unpack_at(eoPtr, index)
        return if (ptr != null) GraphicsEntity.fromPointer(ptr) else null
    }
}
