package io.gitlab.embedSoft.eflKt.ui.widget

import efl.EFL_UI_BUTTON_CLASS
import efl.Eo
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.AutoRepeat
import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.text.DisplayableText
import io.gitlab.embedSoft.eflKt.ui.input.Clickable
import kotlinx.cinterop.CPointer

public actual class Button private constructor(
    ptr: CPointer<Eo>?
) : WidgetBase, DisplayableText, Content, AutoRepeat, Clickable {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual companion object {
        public actual fun create(parent: EflObjectBase): Button =
            Button(EflObject.create(classType = EFL_UI_BUTTON_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Button =
            Button(EflObject.createWithReference(classType = EFL_UI_BUTTON_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun button(parent: EflObjectBase?, withRef: Boolean, init: Button.() -> Unit): Button {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val button = if (!withRef && parent != null) Button.create(parent) else Button.createWithReference(parent)
    button.init()
    return button
}
