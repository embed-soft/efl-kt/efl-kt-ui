package io.gitlab.embedSoft.eflKt.ui.text

import efl.efl_text_get
import efl.efl_text_set
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.toKString

public actual interface DisplayableText : EflObjectBase {
    public actual var text: String
        get() = efl_text_get(eoPtr)?.toKString() ?: ""
        set(value) = efl_text_set(eoPtr, value)
}