package io.gitlab.embedSoft.eflKt.ui.widget

import efl.efl_ui_widget_disabled_get
import efl.efl_ui_widget_disabled_set
import efl.efl_ui_widget_focus_allow_get
import efl.efl_ui_widget_focus_allow_set
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.canvas.CanvasGroupBase

public actual interface WidgetBase : CanvasGroupBase, EflObjectBase {
    public actual var disabled: Boolean
        get() = efl_ui_widget_disabled_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_widget_disabled_set(eoPtr, value.uByteValue)

    public actual var allowFocus: Boolean
        get() = efl_ui_widget_focus_allow_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_widget_focus_allow_set(eoPtr, value.uByteValue)
}
