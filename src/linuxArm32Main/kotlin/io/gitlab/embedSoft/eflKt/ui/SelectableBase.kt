package io.gitlab.embedSoft.eflKt.ui

import efl.efl_ui_selectable_selected_get
import efl.efl_ui_selectable_selected_set
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.booleanValue
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue

public actual interface SelectableBase : EflObjectBase {
    public actual var selected: Boolean
        get() = efl_ui_selectable_selected_get(eoPtr).booleanValue
        set(value) = efl_ui_selectable_selected_set(eoPtr, value.uByteValue)
}
