package io.gitlab.embedSoft.eflKt.ui.widget

import efl.EFL_UI_SLIDER_CLASS
import efl.Eo
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.InteractiveRange
import kotlinx.cinterop.CPointer

public actual class Slider private constructor(ptr: CPointer<Eo>?): WidgetBase, InteractiveRange {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual companion object {
        public actual fun create(parent: EflObjectBase): Slider =
            Slider(EflObject.create(classType = EFL_UI_SLIDER_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Slider =
            Slider(EflObject.createWithReference(classType = EFL_UI_SLIDER_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun slider(parent: EflObjectBase?, withRef: Boolean, init: Slider.() -> Unit): Slider {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val slider = if (!withRef && parent != null) Slider.create(parent) else Slider.createWithReference(parent)
    slider.init()
    return slider
}
