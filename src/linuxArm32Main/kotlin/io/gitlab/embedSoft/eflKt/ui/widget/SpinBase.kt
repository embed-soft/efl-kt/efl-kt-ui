package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.DisplayRange

public actual interface SpinBase : WidgetBase, DisplayRange
