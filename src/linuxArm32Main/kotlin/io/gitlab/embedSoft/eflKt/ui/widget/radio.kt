package io.gitlab.embedSoft.eflKt.ui.widget

import efl.EFL_UI_RADIO_CLASS
import efl.Eo
import efl.efl_ui_radio_state_value_get
import efl.efl_ui_radio_state_value_set
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.CPointer

public actual class Radio private constructor(ptr: CPointer<Eo>?): CheckBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual var stateValue: Int
        get() = efl_ui_radio_state_value_get(eoPtr)
        set(value) = efl_ui_radio_state_value_set(eoPtr, value)

    public actual companion object {
        public actual fun create(parent: EflObjectBase): Radio =
            Radio(EflObject.create(classType = EFL_UI_RADIO_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Radio =
            Radio(EflObject.createWithReference(classType = EFL_UI_RADIO_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun radio(parent: EflObjectBase?, withRef: Boolean, init: Radio.() -> Unit): Radio {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val radio = if (!withRef && parent != null) Radio.create(parent) else Radio.createWithReference(parent)
    radio.init()
    return radio
}
