package io.gitlab.embedSoft.eflKt.ui.canvas

import efl.Eo
import kotlinx.cinterop.CPointer

public actual class CanvasObject private constructor(ptr: CPointer<Eo>?): CanvasObjectBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<Eo>?): CanvasObject = CanvasObject(ptr)
    }
}
