package io.gitlab.embedSoft.eflKt.ui.text

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.toKString

public actual var EflObjectBase.textFontBitmapScalable: UInt
    get() = efl_text_font_bitmap_scalable_get(eoPtr)
    set(value) = efl_text_font_bitmap_scalable_set(eoPtr, value)

public actual var EflObjectBase.textFontFallbacks: String
    get() = efl_text_font_fallbacks_get(eoPtr)?.toKString() ?: ""
    set(value) = efl_text_font_fallbacks_set(eoPtr, value)

public actual var EflObjectBase.textFontLang: String
    get() = efl_text_font_lang_get(eoPtr)?.toKString() ?: ""
    set(value) = efl_text_font_lang_set(eoPtr, value)

public actual var EflObjectBase.textFontSlant: UInt
    get() = efl_text_font_slant_get(eoPtr)
    set(value) = efl_text_font_slant_set(eoPtr, value)

public actual var EflObjectBase.textFontSource: String
    get() = efl_text_font_source_get(eoPtr)?.toKString() ?: ""
    set(value) = efl_text_font_source_set(eoPtr, value)

public actual var EflObjectBase.textFontWeight: UInt
    get() = efl_text_font_weight_get(eoPtr)
    set(value) = efl_text_font_weight_set(eoPtr, value)

public actual var EflObjectBase.textFontWidth: UInt
    get() = efl_text_font_width_get(eoPtr)
    set(value) = efl_text_font_width_set(eoPtr, value)

public actual var EflObjectBase.textFontFamily: String
    get() = efl_text_font_family_get(eoPtr)?.toKString() ?: ""
    set(value) = efl_text_font_family_set(eoPtr, value)

public actual var EflObjectBase.textFontSize: Int
    get() = efl_text_font_size_get(eoPtr)
    set(value) = efl_text_font_size_set(eoPtr, value)
