package io.gitlab.embedSoft.eflKt.ui.input

import efl.efl_input_clickable_interaction_get
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue

public actual interface Clickable : EflObjectBase {
    public actual val clickableInteraction: Boolean
        get() = efl_input_clickable_interaction_get(eoPtr) == true.uByteValue
}
