package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.SelectableBase
import io.gitlab.embedSoft.eflKt.ui.input.Clickable

public actual interface CheckBase : WidgetBase, Content, Clickable, SelectableBase
