package io.gitlab.embedSoft.eflKt.ui.graphics

import efl.efl_gfx_color_code_get
import efl.efl_gfx_color_code_set
import efl.efl_gfx_color_get
import efl.efl_gfx_color_set
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.*

public actual interface Color : EflObjectBase {
    public actual var colorCode: String
        get() = efl_gfx_color_code_get(eoPtr)?.toKString() ?: ""
        set(value) = efl_gfx_color_code_set(eoPtr, value)

    public actual fun getColor(): RgbaColor = memScoped {
        val red = alloc<IntVar>()
        val green = alloc<IntVar>()
        val blue = alloc<IntVar>()
        val alpha = alloc<IntVar>()
        efl_gfx_color_get(obj = eoPtr, r = red.ptr, g = green.ptr, b = blue.ptr, a = alpha.ptr)
        return RgbaColor(red = red.value, green = green.value, blue = blue.value, alpha = alpha.value)
    }

    public actual fun setColor(red: Int, green: Int, blue: Int, alpha: Int) {
        efl_gfx_color_set(obj = eoPtr, r = red, g = green, b = blue, a = alpha)
    }
}
