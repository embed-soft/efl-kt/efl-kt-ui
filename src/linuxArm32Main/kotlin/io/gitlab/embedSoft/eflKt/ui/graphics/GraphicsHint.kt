package io.gitlab.embedSoft.eflKt.ui.graphics

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.booleanValue
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaSize2D
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import kotlinx.cinterop.*

public actual interface GraphicsHint : EflObjectBase {
    public actual var hintAlign: Pair<Double, Double>
        set(value) = efl_gfx_hint_align_set(obj = eoPtr, x = value.first, y = value.second)
        get() = memScoped {
            val xPos = alloc<DoubleVar>()
            val yPos = alloc<DoubleVar>()
            efl_gfx_hint_align_get(obj = eoPtr, x = xPos.ptr, y = yPos.ptr)
            return xPos.value to yPos.value
        }

    public actual var hintAspect: Pair<UInt, EinaSize2D>
        get() = memScoped {
            val mode = alloc<UIntVar>()
            val size = alloc<Eina_Size2D>()
            efl_gfx_hint_aspect_get(obj = eoPtr, mode = mode.ptr, sz = size.ptr)
            mode.value to EinaSize2D.fromPointer(size.ptr)
        }
        set(value) {
            efl_gfx_hint_aspect_set(obj = eoPtr, mode = value.first, sz = value.second.struct.readValue())
        }

    public actual var hintFill: Pair<Boolean, Boolean>
        get() = memScoped {
            val fillX = alloc<UByteVar>()
            val fillY = alloc<UByteVar>()
            efl_gfx_hint_fill_get(obj = eoPtr, x = fillX.ptr, y = fillY.ptr)
            fillX.value.booleanValue to fillY.value.booleanValue
        }
        set(value) {
            efl_gfx_hint_fill_set(obj = eoPtr, x = value.first.uByteValue, y = value.second.uByteValue)
        }

    public actual var hintMargin: GraphicsHintMargin
        get() = memScoped {
            val left = alloc<IntVar>()
            val right = alloc<IntVar>()
            val top = alloc<IntVar>()
            val bottom = alloc<IntVar>()
            efl_gfx_hint_margin_get(obj = eoPtr, l = left.ptr, r = right.ptr, t = top.ptr, b = bottom.ptr)
            GraphicsHintMargin(left = left.value, right = right.value, top = top.value, bottom = bottom.value)
        }
        set(value) {
            efl_gfx_hint_margin_set(obj = eoPtr, l = value.left, r = value.right, t = value.top, b = value.bottom)
        }

    public actual val hintSizeCombinedMax: EinaSize2D
        get() = memScoped {
            EinaSize2D.fromPointer(efl_gfx_hint_size_combined_max_get(eoPtr).ptr)
        }

    public actual val hintSizeCombinedMin: EinaSize2D
        get() = memScoped {
            EinaSize2D.fromPointer(efl_gfx_hint_size_combined_min_get(eoPtr).ptr)
        }

    public actual var hintSizeMax: EinaSize2D
        get() = memScoped {
            EinaSize2D.fromPointer(efl_gfx_hint_size_max_get(eoPtr).ptr)
        }
        set(value) {
            efl_gfx_hint_size_max_set(eoPtr, value.struct.readValue())
        }

    public actual var hintSizeMin: EinaSize2D
        get() = memScoped {
            EinaSize2D.fromPointer(efl_gfx_hint_size_min_get(eoPtr).ptr)
        }
        set(value) {
            efl_gfx_hint_size_min_set(eoPtr, value.struct.readValue())
        }

    public actual var hintWeight: Pair<Double, Double>
        get() = memScoped {
            val x = alloc<DoubleVar>()
            val y = alloc<DoubleVar>()
            efl_gfx_hint_weight_get(obj = eoPtr, x = x.ptr, y = y.ptr)
            x.value to y.value
        }
        set(value) {
            efl_gfx_hint_weight_set(obj = eoPtr, x = value.first, y = value.second)
        }
}
