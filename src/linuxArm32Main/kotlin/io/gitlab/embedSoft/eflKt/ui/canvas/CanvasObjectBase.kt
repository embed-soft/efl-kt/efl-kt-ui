package io.gitlab.embedSoft.eflKt.ui.canvas

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaPosition2D
import io.gitlab.embedSoft.eflKt.core.dataType.toEinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.graphics.Color
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsHint
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsStackBase
import kotlinx.cinterop.readValue

public actual interface CanvasObjectBase : GraphicsEntityBase, Color, GraphicsStackBase, GraphicsHint, EflObjectBase {
    public actual val clippedObjects: EinaIterator
        get() = efl_canvas_object_clipped_objects_get(eoPtr).toEinaIterator()

    public actual var antiAlias: Boolean
        get() = efl_canvas_object_anti_alias_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_anti_alias_set(eoPtr, value.uByteValue)

    public actual val clippedObjectsCount: UInt
        get() = efl_canvas_object_clipped_objects_count(eoPtr)

    public actual var clipper: CanvasObjectBase?
        get() {
            val ptr = efl_canvas_object_clipper_get(eoPtr)
            return if (ptr != null) CanvasObject.fromPointer(ptr) else null
        }
        set(value) = efl_canvas_object_clipper_set(eoPtr, value?.eoPtr)

    public actual var keyFocus: Boolean
        get() = efl_canvas_object_key_focus_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_key_focus_set(eoPtr, value.uByteValue)

    public actual var noRender: Boolean
        get() = efl_canvas_object_no_render_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_no_render_set(eoPtr, value.uByteValue)

    public actual var passEvents: Boolean
        get() = efl_canvas_object_pass_events_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_pass_events_set(eoPtr, value.uByteValue)

    public actual var preciseIsInside: Boolean
        get() = efl_canvas_object_precise_is_inside_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_precise_is_inside_set(eoPtr, value.uByteValue)

    public actual var propagateEvents: Boolean
        get() = efl_canvas_object_propagate_events_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_propagate_events_set(eoPtr, value.uByteValue)

    public actual var repeatEvents: Boolean
        get() = efl_canvas_object_repeat_events_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_object_repeat_events_set(eoPtr, value.uByteValue)

    public actual val seatFocus: Boolean
        get() = efl_canvas_object_seat_focus_get(eoPtr) == true.uByteValue

    public actual fun coordinatesInside(pos: EinaPosition2D): Boolean =
        efl_canvas_object_coords_inside_get(eoPtr, pos.struct.readValue()) == true.uByteValue

    public actual fun keyGrab(keyName: String, modifiers: UInt, notModifiers: UInt, exclusive: Boolean): Boolean =
        efl_canvas_object_key_grab(
            obj = eoPtr,
            keyname = keyName,
            modifiers = modifiers,
            not_modifiers = notModifiers,
            exclusive = exclusive.uByteValue
        ) == true.uByteValue

    public actual fun keyUngrab(keyName: String, modifiers: UInt, notModifiers: UInt) {
        efl_canvas_object_key_ungrab(obj = eoPtr, keyname = keyName, modifiers = modifiers,
            not_modifiers = notModifiers)
    }
}
