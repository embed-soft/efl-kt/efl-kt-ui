package io.gitlab.embedSoft.eflKt.ui.canvas

import efl.Eo
import kotlinx.cinterop.CPointer

public actual class CanvasGroup private constructor(ptr: CPointer<Eo>?): CanvasGroupBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<Eo>?): CanvasGroup = CanvasGroup(ptr)
    }
}
