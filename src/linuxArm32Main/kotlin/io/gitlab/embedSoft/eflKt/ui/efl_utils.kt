package io.gitlab.embedSoft.eflKt.ui

import efl.ecore_con_init
import efl.ecore_con_url_init
import efl.ecore_init_ex
import efl.elm_init
import io.gitlab.embedSoft.eflKt.core.*
import io.gitlab.embedSoft.eflKt.core.event.EflEventHandler
import kotlinx.cinterop.*

/**
 * Starts the Desktop application.
 * @param args The program arguments to use.
 * @param handler The event handler for the main event loop.
 */
public fun startDesktopApplication(args: Array<String>, handler: EflEventHandler) {
    initEcore()
    changeMainLoopHandler(handler)
    addMainLoopHandler()
    // Pass through the program arguments.
    initEcoreEx(args)

    ecore_con_init()
    ecore_con_url_init()
    // Initialize the UI system.
    initElm(args)
    beginMainLoop()
}

public actual fun initElm(args: Array<String>): Int = memScoped {
    // TODO: Fix initializing the Elm library.
    val tmp = if (args.isNotEmpty()) args.toCStringArray(this) else null
    elm_init(args.size, tmp)
}
