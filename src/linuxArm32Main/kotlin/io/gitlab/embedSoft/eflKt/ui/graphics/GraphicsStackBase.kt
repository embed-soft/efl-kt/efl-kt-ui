package io.gitlab.embedSoft.eflKt.ui.graphics

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.CPointer

public actual interface GraphicsStackBase : EflObjectBase {
    public val eflGfxStackPtr: CPointer<Efl_Gfx_Stack>?
        get() = eoPtr
    public actual val above: GraphicsStackBase
        get() = GraphicsStack.fromPointer(efl_gfx_stack_above_get(eoPtr))
    public actual val below: GraphicsStackBase
        get() = GraphicsStack.fromPointer(efl_gfx_stack_below_get(eoPtr))

    public actual var layer: Short
        get() = efl_gfx_stack_layer_get(eoPtr)
        set(value) = efl_gfx_stack_layer_set(eoPtr, value)

    public actual fun lowerToBottom() {
        efl_gfx_stack_lower_to_bottom(eoPtr)
    }

    public actual fun raiseToTop() {
        efl_gfx_stack_raise_to_top(eoPtr)
    }

    public actual fun stackAbove(above: GraphicsStackBase) {
        efl_gfx_stack_above(eoPtr, above.eflGfxStackPtr)
    }

    public actual fun stackBelow(below: GraphicsStackBase) {
        efl_gfx_stack_below(eoPtr, below.eflGfxStackPtr)
    }
}
