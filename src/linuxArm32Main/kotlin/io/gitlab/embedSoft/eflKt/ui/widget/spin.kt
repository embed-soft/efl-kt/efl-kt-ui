package io.gitlab.embedSoft.eflKt.ui.widget

import efl.EFL_UI_SPIN_CLASS
import efl.Eo
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.CPointer

public actual class Spin private constructor(ptr: CPointer<Eo>?): SpinBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual companion object {
        public actual fun create(parent: EflObjectBase): Spin =
            Spin(EflObject.create(classType = EFL_UI_SPIN_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Spin =
            Spin(EflObject.createWithReference(classType = EFL_UI_SPIN_CLASS, parent = parent, init = {}).eoPtr)

    }
}

public actual fun spin(parent: EflObjectBase?, withRef: Boolean, init: Spin.() -> Unit): Spin {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val spin = if (!withRef && parent != null) Spin.create(parent) else Spin.createWithReference(parent)
    spin.init()
    return spin
}
