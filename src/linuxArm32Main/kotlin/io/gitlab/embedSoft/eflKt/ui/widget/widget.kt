package io.gitlab.embedSoft.eflKt.ui.widget

import efl.Efl_Class
import efl.Eo
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.CPointer

public actual class Widget private constructor(ptr: CPointer<Eo>?): WidgetBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<Eo>?): Widget = Widget(ptr)
    }
}

/**
 * Creates a new widget based on an existing [class type][classType]
 * @param classType The type of class to use.
 * @param parent The parent object.
 * @param withRef When *true* an extra reference is created for the widget.
 * @param init A lambda for initializing the widget.
 * @return The new widget.
 */
public fun widget(
    classType: CPointer<Efl_Class>?,
    parent: EflObjectBase?,
    withRef: Boolean = false,
    init: Widget.() -> Unit
): Widget {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val result = if (!withRef && parent != null) {
        Widget.fromPointer(EflObject.create(classType, parent){}.eoPtr)
    } else {
        Widget.fromPointer(EflObject.createWithReference(classType, parent){}.eoPtr)
    }
    result.init()
    return result
}
