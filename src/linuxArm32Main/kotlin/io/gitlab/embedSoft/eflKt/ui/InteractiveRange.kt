package io.gitlab.embedSoft.eflKt.ui

import efl.efl_ui_range_step_get
import efl.efl_ui_range_step_set

public actual interface InteractiveRange : DisplayRange {
    public actual var rangeStep: Double
        get() = efl_ui_range_step_get(eoPtr)
        set(value) = efl_ui_range_step_set(eoPtr, value)
}
