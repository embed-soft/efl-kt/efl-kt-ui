package io.gitlab.embedSoft.eflKt.ui.widget

import efl.EFL_UI_CHECK_CLASS
import efl.Eo
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import kotlinx.cinterop.CPointer

public actual class Check private constructor(ptr: CPointer<Eo>?): CheckBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual companion object {
        public actual fun create(parent: EflObjectBase): Check =
            Check(EflObject.create(classType = EFL_UI_CHECK_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Check =
            Check(EflObject.createWithReference(classType = EFL_UI_CHECK_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun check(parent: EflObjectBase?, withRef: Boolean, init: Check.() -> Unit): Check {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val check = if (!withRef && parent != null) Check.create(parent) else Check.createWithReference(parent)
    check.init()
    return check
}
