package io.gitlab.embedSoft.eflKt.ui.layout

import efl.efl_content_count
import efl.efl_content_iterate
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.toEinaIterator

public actual interface Container : EflObjectBase {
    public actual val contentCount: Int
        get() = efl_content_count(eoPtr)

    public actual fun iterateContent(): EinaIterator = efl_content_iterate(eoPtr).toEinaIterator()
}
