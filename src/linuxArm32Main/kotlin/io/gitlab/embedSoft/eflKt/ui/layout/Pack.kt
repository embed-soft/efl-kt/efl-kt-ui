package io.gitlab.embedSoft.eflKt.ui.layout

import efl.efl_pack
import efl.efl_pack_clear
import efl.efl_pack_unpack
import efl.efl_pack_unpack_all
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase

public actual interface Pack : Container {
    public actual fun pack(subObj: GraphicsEntityBase): Boolean = efl_pack(eoPtr, subObj.eoPtr) == true.uByteValue

    public actual fun clearPackedObjects(): Boolean = efl_pack_clear(eoPtr) == true.uByteValue

    public actual fun unpack(subObj: GraphicsEntityBase): Boolean = efl_pack_unpack(eoPtr, subObj.eoPtr) == true.uByteValue

    public actual fun unpackAll(): Boolean = efl_pack_unpack_all(eoPtr) == true.uByteValue
}
