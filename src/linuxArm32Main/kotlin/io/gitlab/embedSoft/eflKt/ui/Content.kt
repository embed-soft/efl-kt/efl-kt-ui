package io.gitlab.embedSoft.eflKt.ui

import efl.efl_content_get
import efl.efl_content_set
import efl.efl_content_unset
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntity
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase

public actual interface Content : EflObjectBase {
    public actual var content: GraphicsEntityBase?
        get() {
            val ptr = efl_content_get(eoPtr)
            return if (ptr != null) GraphicsEntity.fromPointer(ptr) else null
        }
        set(value) {
            if (value == null) efl_content_unset(eoPtr) else efl_content_set(eoPtr, value.eoPtr)
        }
}
