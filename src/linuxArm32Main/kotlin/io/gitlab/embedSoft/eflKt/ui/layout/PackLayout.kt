package io.gitlab.embedSoft.eflKt.ui.layout

import efl.efl_pack_layout_request
import io.gitlab.embedSoft.eflKt.core.EflObjectBase

public actual interface PackLayout : EflObjectBase {
    public actual fun requestLayout() {
        efl_pack_layout_request(eoPtr)
    }
}
