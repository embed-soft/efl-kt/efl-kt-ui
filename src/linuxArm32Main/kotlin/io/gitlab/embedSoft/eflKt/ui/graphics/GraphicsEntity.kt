package io.gitlab.embedSoft.eflKt.ui.graphics

import efl.Eo
import kotlinx.cinterop.CPointer

/** Default implementation of [GraphicsEntity]. */
public actual class GraphicsEntity private constructor(ptr: CPointer<Eo>?): GraphicsEntityBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<Eo>?): GraphicsEntity = GraphicsEntity(ptr)
    }
}
