package io.gitlab.embedSoft.eflKt.ui.widget

import efl.efl_ui_radio_group_register
import efl.efl_ui_radio_group_selected_value_get
import efl.efl_ui_radio_group_selected_value_set
import efl.efl_ui_radio_group_unregister
import io.gitlab.embedSoft.eflKt.ui.SingleSelectable

public actual interface RadioGroup : SingleSelectable {
    public actual var selectedValue: Int
        get() = efl_ui_radio_group_selected_value_get(eoPtr)
        set(value) = efl_ui_radio_group_selected_value_set(eoPtr, value)

    public actual fun register(radio: Radio) {
        efl_ui_radio_group_register(eoPtr, radio.eoPtr)
    }

    public actual fun unregister(radio: Radio) {
        efl_ui_radio_group_unregister(eoPtr, radio.eoPtr)
    }
}
