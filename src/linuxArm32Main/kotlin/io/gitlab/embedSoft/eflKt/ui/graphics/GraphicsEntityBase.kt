package io.gitlab.embedSoft.eflKt.ui.graphics

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaPosition2D
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaRectangle
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaSize2D
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.readValue

public actual interface GraphicsEntityBase : EflObjectBase {
    public actual var geometry: EinaRectangle
        get() = memScoped {
            EinaRectangle.fromPointer(efl_gfx_entity_geometry_get(eoPtr).ptr)
        }
        set(value) = efl_gfx_entity_geometry_set(eoPtr, value.struct.readValue())

    public actual var position: EinaPosition2D
        get() = memScoped {
            EinaPosition2D.fromPointer(efl_gfx_entity_position_get(eoPtr).ptr)
        }
        set(value) = efl_gfx_entity_position_set(eoPtr, value.struct.readValue())

    public actual var scale: Double
        get() = efl_gfx_entity_scale_get(eoPtr)
        set(value) = efl_gfx_entity_scale_set(eoPtr, value)

    public actual var size: EinaSize2D
        get() = memScoped {
            EinaSize2D.fromPointer(efl_gfx_entity_size_get(eoPtr).ptr)
        }
        set(value) = efl_gfx_entity_size_set(eoPtr, value.struct.readValue())

    public actual var visible: Boolean
        get() = efl_gfx_entity_visible_get(eoPtr) == true.uByteValue
        set(value) = efl_gfx_entity_visible_set(eoPtr, value.uByteValue)
}
