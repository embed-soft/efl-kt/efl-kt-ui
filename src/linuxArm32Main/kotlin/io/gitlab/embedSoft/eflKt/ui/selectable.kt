package io.gitlab.embedSoft.eflKt.ui

import efl.Efl_Ui_Selectable
import efl.Eo
import kotlinx.cinterop.CPointer

public actual class Selectable private constructor(ptr: CPointer<Efl_Ui_Selectable>?): SelectableBase {
    override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<Efl_Ui_Selectable>?): Selectable = Selectable(ptr)
    }
}

public fun CPointer<Efl_Ui_Selectable>?.toSelectable(): Selectable = Selectable.fromPointer(this)
