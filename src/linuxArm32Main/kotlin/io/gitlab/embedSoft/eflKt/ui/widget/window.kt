package io.gitlab.embedSoft.eflKt.ui.widget

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaValue
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.text.DisplayableText
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString

public actual class Window private constructor(ptr: CPointer<Eo>?): WidgetBase, DisplayableText, Content {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual var alpha: Boolean
        get() = efl_ui_win_alpha_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_win_alpha_set(eoPtr, value.uByteValue)

    public actual var exitOnClose: EinaValue
        get() = EinaValue.fromPointer(efl_ui_win_exit_on_close_get(eoPtr))
        set(value) = efl_ui_win_exit_on_close_set(eoPtr, value.einaValuePtr)

    public actual var focusHighlightAnimate: Boolean
        get() = efl_ui_win_focus_highlight_animate_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_win_focus_highlight_animate_set(eoPtr, value.uByteValue)

    public actual var focusHighlightEnabled: Boolean
        get() = efl_ui_win_focus_highlight_enabled_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_win_focus_highlight_enabled_set(eoPtr, value.uByteValue)

    public actual var focusHighlightStyle: String
        get() = efl_ui_win_focus_highlight_style_get(eoPtr)?.toKString() ?: ""
        set(value) {
            efl_ui_win_focus_highlight_style_set(eoPtr, value.ifEmpty { null })
        }

    public actual var fullscreen: Boolean
        get() = efl_ui_win_fullscreen_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_win_fullscreen_set(eoPtr, value.uByteValue)

    public actual var maximized: Boolean
        get() = efl_ui_win_maximized_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_win_maximized_set(eoPtr, value.uByteValue)

    public actual var minimized: Boolean
        get() = efl_ui_win_minimized_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_win_minimized_set(eoPtr, value.uByteValue)

    public actual var winName: String
        get() = efl_ui_win_name_get(eoPtr)?.toKString() ?: ""
        set(value) = efl_ui_win_name_set(eoPtr, value)

    public actual fun activate() {
        efl_ui_win_activate(eoPtr)
    }

    public actual companion object {
        public actual var exitOnAllWindowsClosed: EinaValue
            get() = EinaValue.fromPointer(efl_ui_win_exit_on_all_windows_closed_get())
            set(value) = efl_ui_win_exit_on_all_windows_closed_set(value.einaValuePtr)

        public actual fun create(parent: EflObjectBase): Window =
            Window(EflObject.create(classType = EFL_UI_WIN_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Window =
            Window(EflObject.createWithReference(classType = EFL_UI_WIN_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun window(parent: EflObjectBase?, withRef: Boolean, init: Window.() -> Unit): Window {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val win = if (!withRef && parent != null) Window.create(parent) else Window.createWithReference(parent)
    win.init()
    return win
}
