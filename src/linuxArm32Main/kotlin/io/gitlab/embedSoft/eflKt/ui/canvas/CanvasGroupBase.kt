package io.gitlab.embedSoft.eflKt.ui.canvas

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.toEinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue

public actual interface CanvasGroupBase : CanvasObjectBase, EflObjectBase {
    public actual var groupNeedsRecalculating: Boolean
        get() = efl_canvas_group_need_recalculate_get(eoPtr) == true.uByteValue
        set(value) = efl_canvas_group_need_recalculate_set(eoPtr, value.uByteValue)

    public actual fun calculateGroup() {
        efl_canvas_group_calculate(eoPtr)
    }

    public actual fun changeGroup() {
        efl_canvas_group_change(eoPtr)
    }

    public actual fun addGroupMember(obj: CanvasObjectBase) {
        efl_canvas_group_member_add(eoPtr, obj.eoPtr)
    }

    public actual fun isGroupMember(obj: CanvasObjectBase): Boolean =
        efl_canvas_group_member_is(eoPtr, obj.eoPtr) == true.uByteValue

    public actual fun removeGroupMember(obj: CanvasObjectBase) {
        efl_canvas_group_member_remove(eoPtr, obj.eoPtr)
    }

    public actual fun iterateGroupMembers(): EinaIterator = efl_canvas_group_members_iterate(eoPtr).toEinaIterator()
}
