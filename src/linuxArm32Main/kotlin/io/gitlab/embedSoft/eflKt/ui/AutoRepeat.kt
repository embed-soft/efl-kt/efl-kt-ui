package io.gitlab.embedSoft.eflKt.ui

import efl.*
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue

public actual interface AutoRepeat : EflObjectBase {
    public actual var autoRepeatEnabled: Boolean
        get() = efl_ui_autorepeat_enabled_get(eoPtr) == true.uByteValue
        set(value) = efl_ui_autorepeat_enabled_set(eoPtr, value.uByteValue)

    public actual var autoRepeatGapTimeout: Double
        get() = efl_ui_autorepeat_gap_timeout_get(eoPtr)
        set(value) = efl_ui_autorepeat_gap_timeout_set(eoPtr, value)

    public actual var autoRepeatInitialTimeout: Double
        get() = efl_ui_autorepeat_initial_timeout_get(eoPtr)
        set(value) = efl_ui_autorepeat_initial_timeout_set(eoPtr, value)
}
