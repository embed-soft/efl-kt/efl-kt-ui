package io.gitlab.embedSoft.eflKt.ui.layout

import efl.EFL_UI_BOX_CLASS
import efl.Eo
import efl.efl_ui_box_homogeneous_get
import efl.efl_ui_box_homogeneous_set
import io.gitlab.embedSoft.eflKt.core.EflObject
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.booleanValue
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.ui.widget.WidgetBase
import kotlinx.cinterop.CPointer

public actual class Box private constructor(ptr: CPointer<Eo>?): WidgetBase, PackLinear, PackLayout {
    override val eoPtr: CPointer<Eo>? = ptr

    public actual var homogeneous: Boolean
        get() = efl_ui_box_homogeneous_get(eoPtr).booleanValue
        set(value) = efl_ui_box_homogeneous_set(eoPtr, value.uByteValue)

    public actual companion object {
        public actual fun create(parent: EflObjectBase): Box =
            Box(EflObject.create(classType = EFL_UI_BOX_CLASS, parent = parent, init = {}).eoPtr)

        public actual fun createWithReference(parent: EflObjectBase?): Box =
            Box(EflObject.createWithReference(classType = EFL_UI_BOX_CLASS, parent = parent, init = {}).eoPtr)
    }
}

public actual fun box(parent: EflObjectBase?, withRef: Boolean, init: Box.() -> Unit): Box {
    if (!withRef && parent == null) {
        throw IllegalStateException("The parent parameter cannot be null when withRef is false")
    }
    val box = if (!withRef && parent != null) Box.create(parent) else Box.createWithReference(parent)
    box.init()
    return box
}
