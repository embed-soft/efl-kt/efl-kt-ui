package io.gitlab.embedSoft.eflKt.ui.layout

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * Low level APIs for objects that can lay their children out. Used for containers like `Efl.Ui.Box` and `Efl.Ui.Table`.
 * @since EFL v1.23
 */
public expect interface PackLayout : EflObjectBase {
    /**
     * Requests EFL to recalculate the layout of this object. Internal layout methods might be called asynchronously.
     * @since EFL v1.23
     */
    public open fun requestLayout()
}
