package io.gitlab.embedSoft.eflKt.ui.layout

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator

/**
 * Common interface for objects (containers) that can have multiple contents (sub-objects). APIs in this interface deal
 * with containers of multiple sub-objects, not with individual parts.
 * @since EFL v1.22
 */
public expect interface Container : EflObjectBase {
    /**
     * The number of contained sub-objects.
     * @since EFL v1.22
     */
    public open val contentCount: Int

    /**
     * Begin iterating over this object's contents.
     * @return An iterator.
     * @since EFL v1.22
     */
    public open fun iterateContent(): EinaIterator
}
