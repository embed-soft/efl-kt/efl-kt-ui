package io.gitlab.embedSoft.eflKt.ui.layout

import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase

/**
 * Common interface for objects (containers) with multiple contents (sub-objects) which can be added, and removed at
 * runtime in a linear fashion. This means the sub-objects are internally organized in an ordered list.
 * @since EFL v1.23
 */
public expect interface PackLinear : Pack {
    /**
     * Append an object after the existing sub-object. When this container is deleted it will request
     * deletion of the given [subObj]. Use [unpack] to remove [subObj] from this container without deleting it. If
     * [existing] is *null* then this function behaves like [packEnd].
     * @param subObj The object to pack after [existing].
     * @param existing Existing reference to a sub-object. Must already belong to the container or be *null*.
     * @return A value of *true* if the operation was successful.
     * @since EFL v1.23
     */
    public open fun packAfter(subObj: GraphicsEntityBase, existing: GraphicsEntityBase?): Boolean

    /**
     * Inserts [subObj] **BEFORE** the sub-object at the [index]. An [index] ranges from -count to count-1, where
     * positive numbers go from first sub-object (0) to last (count-1), and negative numbers go from last sub-object
     * (-1) to first (-count). The count is the number of sub-objects currently in the container as returned by
     * [contentCount].
     *
     * If index is less than -count then it will trigger [packBegin] whereas an index greater than count-1 will trigger
     * [packEnd]. When this container is deleted it will request deletion of the given [subObj]. Use [unpack] to remove
     * [subObj] from this container without deleting it.
     * @param subObj The object to pack.
     * @param index The index of existing sub-object to insert **BEFORE**. Valid range is -count to count-1).
     * @return A value of *true* if the operation was successful.
     * @since EFL v1.23
     */
    public open fun packAt(subObj: GraphicsEntityBase, index: Int): Boolean

    /**
     * Prepend an object before the existing sub-object. When this container is deleted it will request deletion of the
     * given [subObj]. Use [unpack] to remove [subObj] from this container without deleting it. If [existing] is *null*
     * then this function behaves like [packBegin].
     * @param subObj The object to pack before [existing].
     * @param existing Existing reference to a sub-object. Must already belong to the container or be *null*.
     * @return A value of *true* if the operation was successful.
     * @since EFL v1.23
     */
    public open fun packBefore(subObj: GraphicsEntityBase, existing: GraphicsEntityBase?): Boolean

    /**
     * Prepend an object at the beginning of this container. This is the same as [packAt] with a *0* index. When this
     * container is deleted it will request deletion of the given [subObj]. Use [unpack] to remove [subObj] from this
     * container without deleting it.
     * @param subObj The object to pack at the beginning.
     * @return A value of *true* if the operation was successful.
     * @since EFL v1.23
     */
    public open fun packBegin(subObj: GraphicsEntityBase): Boolean

    /**
     * Gets the sub-object at a given index in this container. The [index] ranges from -count to count-1, where
     * positive numbers go from first sub-object (0) to last (count-1), and negative numbers go from last sub-object
     * (-1) to first (-count). count is the number of sub-objects currently in the container as returned by
     * [contentCount]. If [index] is less than -count, it will return the first sub-object whereas index greater than
     * count-1 will return the last sub-object.
     * @param index The index of the existing sub-object to retrieve. Valid range is -count to count-1.
     * @return The sub object from this container or *null*.
     * @since EFL v1.23
     */
    public open fun getPackContent(index: Int): GraphicsEntityBase?

    /**
     * Append object at the end of this container. This is the same as [packAt] with a *-1* index. When this container
     * is deleted, it will request deletion of the given [subObj]. Use [unpack] to remove [subObj] from this container
     * without deleting it.
     * @param subObj The object to pack at the end.
     * @return A value of *true* if the operation was successful.
     * @since EFL v1.23
     */
    public open fun packEnd(subObj: GraphicsEntityBase): Boolean

    /**
     * Get the index of a sub-object in this container.
     * @param subObj An existing sub-object in this container.
     * @return The index of [subObj].
     * @since EFL v1.23
     */
    public open fun getPackIndex(subObj: GraphicsEntityBase): Int

    /**
     * Pop out (remove) the sub-object at the specified [index]. An [index] ranges from -count to count-1, where
     * positive numbers go from first sub-object (0) to last (count-1), and negative numbers go from last sub-object
     * (-1) to first (-count). The count is the number of sub-objects currently in the container as returned by
     * [contentCount]. If [index] is less than -count, it will remove the first sub-object whereas [index] greater than
     * count-1 will remove the last sub-object.
     * @param index The index of the sub-object to remove. Valid range is -count to count-1.
     * @return The object that was unpacked or *null*.
     * @since EFL v1.23
     */
    public open fun unpackAt(index: Int): GraphicsEntityBase?
}
