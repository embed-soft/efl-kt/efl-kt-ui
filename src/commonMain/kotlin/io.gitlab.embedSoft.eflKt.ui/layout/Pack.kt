package io.gitlab.embedSoft.eflKt.ui.layout

import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase

/**
 * Common interface for objects (containers) with multiple contents (sub-objects) which can be added, and removed at
 * runtime.
 * @since EFL v1.23
 */
public expect interface Pack : Container {
    /**
     * Adds a [sub-object][subObj] to this container. Depending on the container this will either fill in the default
     * spot, replacing any already existing element or append to the end of the container if there is no default part.
     * When this container is deleted it will request deletion of the given [subObj]. Use [unpack] to remove [subObj]
     * from this container without deleting it.
     * @param subObj The object to pack.
     * @return A value of *true* if [subObj] has been packed.
     * @since EFL v1.23
     */
    public open fun pack(subObj: GraphicsEntityBase): Boolean

    /**
     * Removes all packed sub-objects and unreferences them.
     * @return A value of *true* if this operation was successful.
     * @since EFL v1.23
     */
    public open fun clearPackedObjects(): Boolean

    /**
     * Removes an existing sub-object from the container without deleting it.
     * @param subObj The object to unpack.
     * @return A value of *true* if [subObj] has been unpacked.
     * @since EFL v1.23
     */
    public open fun unpack(subObj: GraphicsEntityBase): Boolean

    /**
     * Removes all packed sub-objects without unreferencing them. Use with caution.
     * @return A value of *true* if this operation was successful.
     * @since EFL v1.23
     */
    public open fun unpackAll(): Boolean
}