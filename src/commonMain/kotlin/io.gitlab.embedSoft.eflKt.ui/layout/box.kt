package io.gitlab.embedSoft.eflKt.ui.layout

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.widget.WidgetBase

/**
 * A container that arranges children widgets in a vertical or horizontal fashion. The Box widget is the most basic
 * (and the most used) of the container widgets. Other widgets are added to the Box through the [PackLinear] interface,
 * and the layout direction (either vertical or horizontal) is controlled through the
 * `Efl.Ui.Layout_Orientable.orientation` property. The Box widget itself is invisible, as are most container widgets:
 * Their purpose is to handle the position and size of all their children so you don't have to.
 *
 * All widgets inside a vertical Box container will have the same width as the container, and their heights will be
 * automatically chosen so that they cover the whole surface of the container from top to bottom (Imagine a stack of
 * pizza boxes neatly fitting inside your oven). The [homogeneous] property then controls whether all widgets have the
 * same height (homogeneous) or not.
 *
 * A horizontal Box container example would be the button toolbar at the top of most word processing programs. Precise
 * layout can be further customized through the `Efl.Gfx.Arrangement` interface on the Box itself, or through the
 * [io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsHint] interface on each of the children widgets.
 * @since EFL v1.23
 */
public expect class Box : WidgetBase, PackLinear, PackLayout {
    /**
     * In homogeneous mode all children of a vertical Box have the same height, equal to the height of the tallest
     * widget. Children of a horizontal Box have the same width, equal to the width of the widest widget. Otherwise
     * individual widget sizes are not modified.
     * @since EFL v1.23
     */
    public var homogeneous: Boolean

    public companion object {
        /**
         * Creates a new [Box] that is owned by [parent].
         * @param parent The parent object that owns the new [Box].
         * @return A new [Box].
         */
        public fun create(parent: EflObjectBase): Box

        /**
         * Creates a new [Box] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Box].
         */
        public fun createWithReference(parent: EflObjectBase?): Box
    }
}

public expect fun box(parent: EflObjectBase?, withRef: Boolean = false, init: Box.() -> Unit): Box
