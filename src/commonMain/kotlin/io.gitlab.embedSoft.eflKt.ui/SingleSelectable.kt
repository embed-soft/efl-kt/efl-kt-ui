package io.gitlab.embedSoft.eflKt.ui

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * An interface for getting access to a single selected item in the implementor. The implementor is free to allow a
 * specific number of selectables being selected or not. This interface just covers the latest selected selectable.
 * @since EFL v1.23
 */
public expect interface SingleSelectable : EflObjectBase {
    /**
     * An object that will be selected in case nothing is selected. Setting an object to this property will be
     * selected instead of no item being selected. This means there will be always at least one element selected. If
     * this property is *null* then the state of **no item is selected** can be reached.
     *
     * Setting this property as a result of selection events results in undefined behavior.
     * @since EFL v1.23
     */
    public open var fallbackSelection: Selectable?

    /** The latest selected item. */
    public open val lastSelected: Selectable
}
