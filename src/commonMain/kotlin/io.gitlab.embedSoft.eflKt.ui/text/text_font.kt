package io.gitlab.embedSoft.eflKt.ui.text

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * The bitmap fonts have fixed size glyphs for several available sizes. Basically it is not scalable. But it needs to
 * be scalable for some use cases. (eg colorful emoji fonts).
 */
public expect var EflObjectBase.textFontBitmapScalable: UInt

/** Comma-separated list of font fallbacks. */
public expect var EflObjectBase.textFontFallbacks: String

/** Specific language of the displayed font. */
public expect var EflObjectBase.textFontLang: String

/** Type of slant of the displayed font. */
public expect var EflObjectBase.textFontSlant: UInt

/** The font source file to be used on a given text object. */
public expect var EflObjectBase.textFontSource: String

/** Type of weight of the displayed font. */
public expect var EflObjectBase.textFontWeight: UInt

/** Type of width of the displayed font. */
public expect var EflObjectBase.textFontWidth: UInt

/** The font family. */
public expect var EflObjectBase.textFontFamily: String

/** The font size. */
public expect var EflObjectBase.textFontSize: Int
