package io.gitlab.embedSoft.eflKt.ui.text

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/** The text markup (in HTML form) that is applied to a given text object. */
public expect var EflObjectBase.textMarkup: String
