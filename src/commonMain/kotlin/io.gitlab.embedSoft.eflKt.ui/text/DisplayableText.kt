package io.gitlab.embedSoft.eflKt.ui.text

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * Indicates that the object can display text.
 * @since EFL v1.22
 */
public expect interface DisplayableText : EflObjectBase {
    /**
     * The text string to be displayed by the given object.
     * @since EFL v1.22
     */
    public open var text: String
}