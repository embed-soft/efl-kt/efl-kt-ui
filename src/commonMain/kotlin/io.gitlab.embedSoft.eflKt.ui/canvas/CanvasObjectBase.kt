package io.gitlab.embedSoft.eflKt.ui.canvas

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaPosition2D
import io.gitlab.embedSoft.eflKt.ui.graphics.Color
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsHint
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsStackBase

/**
 * Contains functionality for a canvas object.
 * @since EFL v1.22
 */
public expect interface CanvasObjectBase : GraphicsEntityBase, Color, GraphicsStackBase, GraphicsHint, EflObjectBase {
    public open val clippedObjects: EinaIterator

    /**
     * Whether or not the given Evas object is to be drawn anti-aliased.
     * @since EFL v1.22
     */
    public open var antiAlias: Boolean

    /**
     * The number of objects clipped by this canvas object.
     * @since EFL v1.22
     */
    public open val clippedObjectsCount: UInt

    /**
     * Clip one object to another. This property will clip this canvas object to the area occupied by the object clip.
     * This means this canvas object will only be visible within the area occupied by the clipping object (clip). The
     * color of the object being clipped will be multiplied by the color of the clipping one, so the resulting color
     * for the former will be "RESULT = (OBJ * CLIP) / (255 * 255)", per color element (red, green, blue and alpha).
     *
     * Clipping is recursive, so clipping objects may be clipped by others, and their color will in term be multiplied.
     * You may not set up circular clipping lists (i.e. object 1 clips object 2, which clips object 1). Objects which
     * do not clip others are visible in the canvas as normal; those that clip one or more objects become invisible
     * themselves, only affecting what they clip. If an object ceases to have other objects being clipped by it, it
     * will become visible again.
     *
     * The visibility of an object affects the objects that are clipped by it, so if the object clipping others isn't
     * shown (as in Efl.Gfx.Entity.visible) the objects clipped by it will not be shown either. If this canvas object
     * was being clipped by another object when this property is used, it gets implicitly removed from the old
     * clipper's domain and is made now to be clipped by its new clipper.
     *
     * If clip is null then this call will disable clipping for the object i.e. its visibility, and color get detached
     * from the previous clipper. If it wasn't then this has no effect.
     * @since EFL v1.22
     */
    public open var clipper: CanvasObjectBase?

    /**
     * Indicates that this object is the keyboard event receiver on its canvas. Changing focus only affects where
     * (key) input events go. There can be only one object focused at any time. If this property is true then this
     * canvas object will be set as the currently focused object, and it will receive all keyboard events that are not
     * exclusive key grabs on other objects.
     * @since EFL v1.22
     */
    public open var keyFocus: Boolean

    /**
     * Disables all rendering on the canvas. This flag will be used to indicate to Evas that this object should
     * **NEVER** be rendered on the canvas under any circumstances. In particular this is useful to avoid drawing
     * clipper objects (or masks) even when they don't clip any object. This can also be used to replace the old
     * `sources_visible` flag with proxy objects.
     *
     * This is different to the [visible] property, as even visible objects marked as **no-render** will never appear
     * on screen. But those objects can still be used as proxy sources or clippers. When hidden all **no-render**
     * objects will completely disappear from the canvas, and hide their clippees or be invisible when used as proxy
     * sources.
     * @since EFL v1.22
     */
    public open var noRender: Boolean

    /**
     * Whether an Evas object is to pass (ignore) events. If this property is *true* then it will make events on this
     * canvas object ignored. They will be triggered on the next lower object (that is not set to pass events)
     * instead. If this property is *false* then events will be processed on that object as normal.
     * @see repeatEvents
     * @see propagateEvents
     * @see GraphicsStackBase
     * @since EFL v1.22
     */
    public open var passEvents: Boolean

    /**
     * Whether to use precise (usually expensive) point collision detection for a given Evas object. Use this property
     * to make Evas treat object transparent areas as not belonging to it with regard to mouse pointer events. By
     * default all of the object's boundary rectangle will be taken in account for them.
     * @since EFL v1.22
     */
    public open var preciseIsInside: Boolean

    /**
     * Whether events on a smart object's member should be propagated up to its parent. This property has no effect if
     * this canvas object isn't a member of a smart object. If this property is *true* then events occurring on this
     * object will be propagated on to the smart object of which obj is a member. However if this property is *false*
     * then events occurring on this object will not be propagated on to the smart object of which obj is a member.
     * @see repeatEvents
     * @see passEvents
     * @since EFL v1.22
     */
    public open var propagateEvents: Boolean

    /**
     * Whether an Evas object is to repeat events to objects below it. If this property is *true* then it will make
     * events on obj to also be repeated for the next lower object in the object's stack. However if this property is
     * *false* then events occurring on this canvas object will be processed only on it.
     * @since EFL V1.22
     * @see GraphicsStackBase
     */
    public open var repeatEvents: Boolean

    /** Whether focused by at least one seat. */
    public open val seatFocus: Boolean

    /**
     * Checks to see if there are coordinates inside this canvas object.
     * @param pos The coordinates in pixels.
     * @return A value of *true* if the coordinates are inside the object, *false* otherwise.
     */
    public open fun coordinatesInside(pos: EinaPosition2D): Boolean

    /**
     * Requests [keyName] key events be directed to this canvas object. Key grabs allow one or more objects to receive
     * key events for specific key strokes even if other objects have focus. Whenever a key is grabbed only the objects
     * grabbing it will get the events for the given keys. Note that [keyName] is a platform dependent symbolic name
     * for the key pressed.
     *
     * The [modifiers] and [notModifiers] parameters are bit masks of all the modifiers that must and mustn't,
     * respectively, be pressed along with [keyName] key in order to trigger this new key grab. Modifiers can be
     * things such as **Shift** and **Ctrl** as well as user defined types via `@ref evas_key_modifier_add`. The
     * [exclusive] parameter will make the given object the only one permitted to grab the given key. If given *true*
     * then subsequent calls on this function with different canvas object arguments will fail, unless the key is
     * un-grabbed again.
     * @param keyName The key to request events for.
     * @param modifiers A combination of modifier keys that must be present to trigger the event.
     * @param notModifiers A combination of modifier keys that must not be present to trigger the event.
     * @param exclusive Request that this canvas object is the only object receiving the [keyName] events.
     * @return A value of *true* if the operation was successful.
     * @since EFL v1.22
     */
    public open fun keyGrab(keyName: String, modifiers: UInt, notModifiers: UInt, exclusive: Boolean): Boolean

    /**
     * Removes the grab on [keyName] key events by this canvas object. Removes a key grab on obj if [keyName],
     * [modifiers], and [notModifiers] match.
     * @see keyGrab
     * @see keyFocus
     * @since EFL v1.22
     */
    public open fun keyUngrab(keyName: String, modifiers: UInt, notModifiers: UInt)
}