package io.gitlab.embedSoft.eflKt.ui.canvas

/** Default implementation of [CanvasObjectBase]. */
public expect class CanvasObject : CanvasObjectBase
