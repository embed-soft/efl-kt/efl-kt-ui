package io.gitlab.embedSoft.eflKt.ui.canvas

/** Default implementation of [CanvasGroupBase]. */
public expect class CanvasGroup : CanvasGroupBase
