package io.gitlab.embedSoft.eflKt.ui.canvas

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator

/**
 * A group object is a container for other canvas objects. Its children move along their parent and are often clipped
 * with a common clipper. This is part of the legacy smart object concept. Note that a group isn't necessarily a
 * container in the sense that a standard widget may not have any empty event handlers for content. However it's still
 * a group of low level canvas objects (clipper, raw objects, etc.).
 * @since EFL v1.22
 */
public expect interface CanvasGroupBase : CanvasObjectBase, EflObjectBase {
    /**
     * Indicates that the group's layout needs to be recalculated. If this flag is set then the
     * [calculateGroup] function will be called, during rendering phase of the canvas. After that this flag will be
     * automatically unset.
     * @since EFL v1.22
     */
    public open var groupNeedsRecalculating: Boolean

    /**
     * Gets an iterator over the children of this object, which are canvas objects. This returns the list of **smart**
     * children. This might be different from both the [EflObjectBase] children list as well as the `Efl.Container`
     * content list.
     * @return An iterator to iterate over all the group members.
     * @since EFL v1.22
     */
    public open fun iterateGroupMembers(): EinaIterator

    /**
     * Triggers an immediate recalculation of this object's geometry. This will also reset the flag
     * [groupNeedsRecalculating].
     * @since EFL v1.22
     */
    public open fun calculateGroup()

    /**
     * Marks the object as dirty. This also forcefully marks the given object as needing recalculation. As an effect
     * on the next rendering cycle its [calculateGroup] function will be called.
     * @since EFL v1.22
     */
    public open fun changeGroup()

    /**
     * Set a canvas object as a member of a given group (or smart object). Members will automatically be stacked, and
     * layered together with the smart object. The various stacking functions will operate on members relative to the
     * other members instead of the entire canvas, since they now live on an exclusive layer.
     *
     * Subclasses inheriting from this one may override this function to ensure the proper stacking of special objects,
     * such as clippers, event rectangles, etc...
     * @param obj The object to add.
     * @see io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsStackBase.stackAbove
     * @see removeGroupMember
     * @see isGroupMember
     * @since EFL v1.22
     */
    public open fun addGroupMember(obj: CanvasObjectBase)

    /**
     * Finds out if a given object is a member of this group.
     * @param obj The object to check.
     * @return A value of *true* if [obj] is a group member.
     * @since EFL v1.22
     */
    public open fun isGroupMember(obj: CanvasObjectBase): Boolean

    /**
     * Removes a member object from a given smart object. This removes a member object from a smart object, if it was
     * added to any. The object will still be on the canvas, but no longer associated with whichever smart object it
     * was associated with.
     * @param obj The object to remove.
     * @see addGroupMember
     * @see isGroupMember
     * @since EFL v1.22
     */
    public open fun removeGroupMember(obj: CanvasObjectBase)
}