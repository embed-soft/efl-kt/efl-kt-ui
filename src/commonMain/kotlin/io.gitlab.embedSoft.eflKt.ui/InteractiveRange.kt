package io.gitlab.embedSoft.eflKt.ui

/**
 * An interface that extends the normal displaying properties with usage properties. The properties defined here are
 * used to manipulate the way a user interacts with a displayed range.
 * @since EFL v1.23
 */
public expect interface InteractiveRange : DisplayRange {
    /**
     * Control the step used to increment or decrement values for given widget. This value will be incremented or
     * decremented to the displayed value. By default step value is equal to *1*.
     * @since EFL v1.23
     */
    public open var rangeStep: Double
}
