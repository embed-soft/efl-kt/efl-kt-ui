package io.gitlab.embedSoft.eflKt.ui

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.graphics.GraphicsEntityBase

/**
 * Common interface for objects that have a single sub-object as content.
 * @since EFL v1.22
 */
public expect interface Content : EflObjectBase {
    /**
     * The Sub-object currently set as this object's single content. Setting this property to *null* will unset the
     * object's content.
     * @since EFL v1.22
     */
    public open var content: GraphicsEntityBase?
}
