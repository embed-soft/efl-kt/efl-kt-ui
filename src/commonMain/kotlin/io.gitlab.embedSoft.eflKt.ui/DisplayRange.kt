package io.gitlab.embedSoft.eflKt.ui

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * Interface that contains properties regarding the displaying of a value within a range. A value range contains a
 * value restricted between specified minimum and maximum limits at all times. This can be used for progress bars,
 * sliders or spinners, for example.
 * @since EFL v1.23
 */
public expect interface DisplayRange : EflObjectBase {
    /**
     * The minimum and maximum values for given range widget. If the current value is less than min, it will be updated
     * to min. If it is bigger then max, will be updated to max. The resulting value can be obtained with
     * [rangeValue].
     *
     * The default minimum and maximum values may be different for each class. This property is a Pair containing the
     * following:
     * 1. min - Minimum value.
     * 2. max - Maximum value.
     * @since EFL v1.23
     */
    public open var rangeLimits: Pair<Double, Double>

    /**
     * Control the value (position) of the widget within its valid range. Values outside the limits defined in
     * [rangeLimits] are ignored, and an error is printed.
     * @since EFL v1.23
     */
    public open var rangeValue: Double
}
