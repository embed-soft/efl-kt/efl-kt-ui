package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.canvas.CanvasGroupBase

/**
 * Base interface for all widgets. This interface is designed in a way that widgets can be expressed as a tree. The
 * parent relation should **NEVER** be modified directly. Instead you should use the APIs of the widgets
 * (Typically `Efl.Pack_Linear`, `Efl.Pack_Table` or `Efl.Content`).
 *
 * Properties implemented here should be treated with extra care. Some are defined for the sub-tree, others are
 * defined for the widget itself.
 * @since EFL v1.22
 */
public expect interface WidgetBase : CanvasGroupBase, EflObjectBase {
    /**
     * Whether the widget is enabled (accepts and reacts to user inputs). Each widget may handle the disabled state
     * differently, but overall disabled widgets shall not respond to any input events. This is *false* by default,
     * meaning the widget is enabled. Disabling a widget will disable all its children recursively, but only this
     * widget will be marked as disabled internally.
     *
     * This property will be *true* if any widget in the parent hierarchy is disabled. Re-enabling that parent may in
     * turn change the disabled state of this widget.
     * @since EFL v1.22
     */
    public open var disabled: Boolean

    /**
     * The ability for a widget to be focused. Unfocusable objects do nothing when programmatically focused. The
     * nearest focusable parent object the one really getting focus. Also when they receive mouse input they will get
     * the event, but not take away the focus from where it was previously.
     * @since EFL v1.22
     */
    public open var allowFocus: Boolean
}
