package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/** Default implementation of [SpinBase]. */
public expect class Spin : SpinBase {
    public companion object {
        /**
         * Creates a new [Spin] that is owned by [parent].
         * @param parent The parent object that owns the new [Spin].
         * @return A new [Spin].
         */
        public fun create(parent: EflObjectBase): Spin

        /**
         * Creates a new [Spin] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Spin].
         */
        public fun createWithReference(parent: EflObjectBase?): Spin
    }
}

public expect fun spin(parent: EflObjectBase?, withRef: Boolean = false, init: Spin.() -> Unit): Spin
