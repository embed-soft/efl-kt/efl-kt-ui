package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.InteractiveRange

/**
 * A spin button widget, which allows the user to increase or decrease numeric values using the arrow buttons, or to
 * edit values directly by clicking over them and inputting new ones.
 * @since EFL v1.23
 */
public expect class SpinButton : SpinBase, InteractiveRange {
    /**
     * Control whether the spin should circulate value when it reaches its minimum or maximum value. Disabled by
     * default. If disabled when the user tries to increment the value, but displayed value plus step value is bigger
     * than maximum value, then the new value will be the maximum value. The same happens when the user tries to
     * decrement it, but the value less step is less than minimum value. In this case the new displayed value will be
     * the minimum value.
     *
     * If enabled when the user tries to increment the value, but displayed value plus step value is bigger than
     * maximum value, the new value will become the minimum value. When the user tries to decrement it, if the value
     * minus step is less than minimum value, then the new displayed value will be the maximum value:
     * ```
     * E.g.: min = 10 max = 50 step = 20 displayed = 20
     * ```
     *
     * When the user decrements the value (using left or bottom arrow) it will display **$50**.
     * @since EFL v1.23
     */
    public var wrapAround: Boolean

    public companion object {
        /**
         * Creates a new [SpinButton] that is owned by [parent].
         * @param parent The parent object that owns the new [SpinButton].
         * @return A new [SpinButton].
         */
        public fun create(parent: EflObjectBase): SpinButton

        /**
         * Creates a new [SpinButton] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [SpinButton].
         */
        public fun createWithReference(parent: EflObjectBase?): SpinButton
    }
}

public expect fun spinButton(parent: EflObjectBase?, withRef: Boolean = false, init: SpinButton.() -> Unit): SpinButton
