package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.text.DisplayableText
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.AutoRepeat
import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.input.Clickable

/**
 * Text box widget.
 */
public expect class TextBox : WidgetBase, DisplayableText, Content, AutoRepeat, Clickable {
    public companion object {
        /**
         * Creates a new [TextBox] that is owned by [parent].
         * @param parent The parent object that owns the new [TextBox].
         * @return A new [TextBox].
         */
        public fun create(parent: EflObjectBase): TextBox

        /**
         * Creates a new [TextBox] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Button].
         */
        public fun createWithReference(parent: EflObjectBase?): TextBox
    }
}

public expect fun textBox(parent: EflObjectBase?, withRef: Boolean = false, init: TextBox.() -> Unit): TextBox
