package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.text.DisplayableText
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.AutoRepeat
import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.input.Clickable

/**
 * Push button widget. Press it and run some function. It can contain a simple label, and icon object. The widget also
 * has an **auto repeat** feature.
 *
 * The icon can be set using [content] and the text can be set using [text]. All events of [Clickable] can be used to
 * listen to a **click** event from the user.
 * @since EFL v1.23
 */
public expect class Button : WidgetBase, DisplayableText, Content, AutoRepeat, Clickable {
    public companion object {
        /**
         * Creates a new [Button] that is owned by [parent].
         * @param parent The parent object that owns the new [Button].
         * @return A new [Button].
         */
        public fun create(parent: EflObjectBase): Button

        /**
         * Creates a new [Button] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Button].
         */
        public fun createWithReference(parent: EflObjectBase?): Button
    }
}

public expect fun button(parent: EflObjectBase?, withRef: Boolean = false, init: Button.() -> Unit): Button
