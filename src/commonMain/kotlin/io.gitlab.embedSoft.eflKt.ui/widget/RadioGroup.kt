package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.SingleSelectable

/**
 * An interface for manually handling a group of [Radio] buttons.
 * @see Radio
 * @since EFL v1.23
 */
public expect interface RadioGroup : SingleSelectable {
    /**
     * The value associated with the currently selected button in the group. Give each radio button in the group a
     * different value using [Radio.stateValue]. A value of *-1* means that no button is selected. Only values
     * associated with the buttons in the group (and *-1*) can be used.
     * @since EFL v1.23
     */
    public open var selectedValue: Int

    /**
     * Register a new [Radio button][Radio] to this group. Keep in mind that registering to a group will only handle
     * button grouping, you still need to add the button to a layout for it to be rendered. If the [Radio.stateValue]
     * of the new button is already used by a previous button in the group then the button will not be added.
     * @param radio The radio button to add to the group.
     * @since EFL v1.23
     * @see unregister
     */
    public open fun register(radio: Radio)

    /**
     * Unregister an [Radio button][Radio] from this group. This will unlink the behavior of this button from the other
     * buttons in the group, but if it still belongs to a layout then it will still be rendered. If the button was not
     * registered in the group then the call is ignored. If the button was selected then no button will be selected in
     * the group after this call.
     * @param radio The radio button to remove from the group.
     * @since EFL v1.23
     * @see register
     */
    public open fun unregister(radio: Radio)
}
