package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.SelectableBase
import io.gitlab.embedSoft.eflKt.ui.input.Clickable

/**
 * Check widget. The check widget allows for toggling a value between *true* and *false*. Check objects are a lot like
 * `Efl.Ui.Radio` objects in layout and functionality, except they do not work as a group, but independently and only
 * toggle the value of a boolean between *false* and *true*. The boolean value of the check can be retrieved using the
 * [selected] property. Changes to [selected] can be listed to using the `Efl.Ui.SelectableBase.selected,changed` event.
 * @since EFL v1.23
 */
public expect interface CheckBase : WidgetBase, Content, Clickable, SelectableBase
