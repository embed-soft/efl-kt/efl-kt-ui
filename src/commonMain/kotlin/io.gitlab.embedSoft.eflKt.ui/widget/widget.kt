package io.gitlab.embedSoft.eflKt.ui.widget

/** Default implementation of [WidgetBase]. */
public expect class Widget : WidgetBase
