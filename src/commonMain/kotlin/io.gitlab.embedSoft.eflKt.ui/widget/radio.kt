package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * A radio button. Radio buttons are like check boxes in that they can be either checked or unchecked. However radio
 * buttons are always bunched together in groups, and only one button in each group can be checked at any given time.
 * Pressing a different button in the group will automatically uncheck any previously checked button. They are a common
 * way to allow a user to select one option among a list.
 *
 * To handle button grouping you can either use an `Efl.Ui.Radio_Group_Impl` object, or use more convenient widgets
 * like `Efl.Ui.Radio_Box`.
 * @since EFL v1.23
 */
public expect class Radio : CheckBase {
    /**
     * An [Int] value that this radio button represents. Each radio button in a group **MUST** have a unique value. The
     * selected button in a group can then be set, or retrieved through the [RadioGroup.selectedValue] property. This
     * value is also informed through the `Efl.Ui.Radio_Group.value,changed` event.
     *
     * All non-negative values are legal but keep in mind that *0* is the starting value for all new groups. If no
     * button in the group has this value then no button in the group is initially selected; *-1* is the value that
     * [RadioGroup.selectedValue] returns when no button is selected and therefore cannot be used.
     * @since EFL v1.23
     */
    public var stateValue: Int

    public companion object {
        /**
         * Creates a new [Radio] that is owned by [parent].
         * @param parent The parent object that owns the new [Radio].
         * @return A new [Radio].
         */
        public fun create(parent: EflObjectBase): Radio

        /**
         * Creates a new [Radio] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Radio].
         */
        public fun createWithReference(parent: EflObjectBase?): Radio
    }
}

public expect fun radio(parent: EflObjectBase?, withRef: Boolean = false, init: Radio.() -> Unit): Radio
