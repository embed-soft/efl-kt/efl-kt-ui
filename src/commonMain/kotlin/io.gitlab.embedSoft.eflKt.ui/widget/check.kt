package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/** Default implementation of [CheckBase]. */
public expect class Check : CheckBase {
    public companion object {
        /**
         * Creates a new [Check] that is owned by [parent].
         * @param parent The parent object that owns the new [Check].
         * @return A new [Check].
         */
        public fun create(parent: EflObjectBase): Check

        /**
         * Creates a new [Check] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Check].
         */
        public fun createWithReference(parent: EflObjectBase?): Check
    }
}

public expect fun check(parent: EflObjectBase?, withRef: Boolean = false, init: Check.() -> Unit): Check
