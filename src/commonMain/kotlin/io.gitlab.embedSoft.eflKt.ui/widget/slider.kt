package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.ui.InteractiveRange

/**
 * A slider widget. This lets the user select a numerical value inside the
 * [limits][io.gitlab.embedSoft.eflKt.ui.DisplayRange.rangeLimits]. The current value can be retrieved using the
 * [InteractiveRange] interface. Events monitoring its changes are also available in that interface. The visual
 * representation of min and max can be swapped using `Efl.Ui.Layout_Orientable.orientation`. Normally the minimum of
 * [io.gitlab.embedSoft.eflKt.ui.DisplayRange.rangeLimits] is shown on the left side, the max on the right side.
 * @since EFL v1.23
 */
public expect class Slider : WidgetBase, InteractiveRange {
    public companion object {
        /**
         * Creates a new [Slider] that is owned by [parent].
         * @param parent The parent object that owns the new [Slider].
         * @return A new [Slider].
         */
        public fun create(parent: EflObjectBase): Slider

        /**
         * Creates a new [Slider] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Slider].
         */
        public fun createWithReference(parent: EflObjectBase?): Slider
    }
}

public expect fun slider(parent: EflObjectBase?, withRef: Boolean = false, init: Slider.() -> Unit): Slider
