package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.EinaValue
import io.gitlab.embedSoft.eflKt.ui.Content
import io.gitlab.embedSoft.eflKt.ui.text.DisplayableText

/**
 * Represents a window.
 * @since EFL v1.22
 */
public expect class Window : WidgetBase, DisplayableText, Content {
    /**
     * The alpha channel state of this window. If this property is *true* then the alpha channel of the canvas will be
     * enabled, possibly making parts of the window completely or partially transparent. This is also subject to the
     * underlying system supporting it, for example a system using a compositing manager.
     * @since EFL v1.22
     */
    public var alpha: Boolean

    /**
     * Enable quitting the main loop when this window is closed. When set the window's loop object will exit using the
     * passed exit code if the window is closed. The value passed should be empty to unset this state, or an Int value
     * to be used as the exit code.
     *
     * Note this is different from [exitOnAllWindowsClosed] which exits when **ALL** windows are closed.
     * @since EFL v1.22
     */
    public var exitOnClose: EinaValue

    /**
     * Whether focus highlight should animate or not.
     * @see focusHighlightStyle
     * @see focusHighlightEnabled
     * @since EFL v1.22
     */
    public var focusHighlightAnimate: Boolean

    /**
     * Whether focus highlight is enabled or not on this window, regardless of the global setting.
     * @see focusHighlightStyle
     * @see focusHighlightAnimate
     * @since EFL v1.22
     */
    public var focusHighlightEnabled: Boolean

    /**
     * Control the widget focus highlight style. If this property is *""* (an empty [String]) then the default will be
     * used.
     * @see focusHighlightEnabled
     * @see focusHighlightAnimate
     * @since EFL v1.22
     */
    public var focusHighlightStyle: String

    /**
     * The full screen state of a window.
     * @since EFL v1.22
     */
    public var fullscreen: Boolean

    /**
     * The maximized state of a window.
     * @since EFL v1.22
     */
    public var maximized: Boolean

    /**
     * The minimized state of a window.
     * @since EFL v1.22
     */
    public var minimized: Boolean

    /**
     * The window name. Note that the meaning of name depends on the underlying windowing system. This property is a
     * construction property that can only be set at creation time.
     * @since EFL v1.22
     */
    public var winName: String

    /**
     * Activates this window. This function sends a request to the Window Manager to activate the window. If honored by
     * the Window Manager then the window will receive the keyboard focus.
     * @since EFL v1.22
     */
    public fun activate()

    public companion object {
        /**
         * Enable quitting the main loop when all windows are closed. When set the main loop will quit with the passed
         * exit code once all windows have been closed. The value passed should be empty to unset this state, or an
         * Int value to be used as the exit code.
         *
         * Note this is different from `exitOnClose` which exits when a given window is closed.
         * @since EFL v1.22
         */
        public var exitOnAllWindowsClosed: EinaValue

        /**
         * Creates a new [Window] that is owned by [parent].
         * @param parent The parent object that owns the new [Window].
         * @return A new [Window].
         */
        public fun create(parent: EflObjectBase): Window

        /**
         * Creates a new [Window] that has an extra reference.
         * @param parent The parent object, may be *null*.
         * @return A new [Window].
         */
        public fun createWithReference(parent: EflObjectBase?): Window
    }
}

public expect fun window(parent: EflObjectBase?, withRef: Boolean = false, init: Window.() -> Unit): Window
