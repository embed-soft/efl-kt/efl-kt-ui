package io.gitlab.embedSoft.eflKt.ui.widget

import io.gitlab.embedSoft.eflKt.ui.DisplayRange

/**
 * An interface representing a spin widget, which allows the user to increase or decrease a numeric value using arrow
 * buttons. It's a basic type of widget for choosing and displaying values.
 * @since EFL v1.23
 */
public expect interface SpinBase : WidgetBase, DisplayRange
