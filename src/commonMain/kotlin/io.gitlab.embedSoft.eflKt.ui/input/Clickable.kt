package io.gitlab.embedSoft.eflKt.ui.input

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * Provides click functionality for an object.
 * @since EFL v1.23
 */
public expect interface Clickable : EflObjectBase {
    /** Whether the object can be clicked. */
    public open val clickableInteraction: Boolean
}
