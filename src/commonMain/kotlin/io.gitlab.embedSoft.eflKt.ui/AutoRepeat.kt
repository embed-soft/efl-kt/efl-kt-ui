package io.gitlab.embedSoft.eflKt.ui

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * Interface for auto repeating clicks. This interface abstracts functions for enabling / disabling this feature. When
 * enabled, keeping a button pressed will continuously fire the repeated event until the button is released. The time
 * it takes until it starts emitting the event is given by [autoRepeatInitialTimeout], and the time between each new
 * emission by [autoRepeatGapTimeout].
 * @since EFL v1.23
 */
public expect interface AutoRepeat : EflObjectBase {
    /**
     * Turn on/off the **autorepeat** event generated when a button is kept pressed. When off, no **autorepeat** is
     * performed, and buttons fire a normal **clicked** event when they are clicked.
     * @since EFL v1.23
     */
    public open var autoRepeatEnabled: Boolean

    /**
     * The interval between each generated **autorepeat** event in seconds.
     * @since EFL v1.23
     */
    public open var autoRepeatGapTimeout: Double

    /**
     * The initial timeout before the **autorepeat** event is generated. Sets the timeout in seconds, since the button
     * is pressed until the first repeated event is emitted. If the value is *0.0* or less, there won't be any delay
     * and the event will be fired the moment the button is pressed.
     * @see autoRepeatEnabled
     * @see autoRepeatGapTimeout
     * @since EFL v1.23
     */
    public open var autoRepeatInitialTimeout: Double
}
