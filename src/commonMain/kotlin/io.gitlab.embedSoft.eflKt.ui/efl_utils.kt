package io.gitlab.embedSoft.eflKt.ui

/**
 * Initializes the Elm library (used for GUI stuff).
 * @param args The array containing the program arguments to pass through.
 * @return A value of *0* on success. Any other value indicates failure.
 */
public expect fun initElm(args: Array<String>): Int
