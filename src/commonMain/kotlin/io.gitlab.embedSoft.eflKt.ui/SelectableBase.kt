package io.gitlab.embedSoft.eflKt.ui

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * SelectableBase interface for UI objects. An object implementing this interface can be selected. When the [selected]
 * property of this object changes the `Efl.Ui.SelectableBase.selected,changed` event is fired.
 * @since EFL v1.23
 */
public expect interface SelectableBase : EflObjectBase {
    /**
     * The selected state of this object A change to this property emits the changed event.
     * @since EFL v1.23
     */
    public open var selected: Boolean
}
