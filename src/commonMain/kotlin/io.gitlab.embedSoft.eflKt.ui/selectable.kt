package io.gitlab.embedSoft.eflKt.ui

/** Default implementation of [SelectableBase]. */
public expect class Selectable : SelectableBase
