package io.gitlab.embedSoft.eflKt.ui.graphics

/** Provides graphics hint margin metadata. */
public data class GraphicsHintMargin(
    /** Left padding. */
    val left: Int,
    /** Right padding */
    val right: Int,
    /** Top margin. */
    val top: Int,
    /** Bottom padding. */
    val bottom: Int
)
