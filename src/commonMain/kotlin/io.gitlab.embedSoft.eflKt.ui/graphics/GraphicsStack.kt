package io.gitlab.embedSoft.eflKt.ui.graphics

/** Default implementation of [GraphicsStackBase] */
public expect class GraphicsStack : GraphicsStackBase
