package io.gitlab.embedSoft.eflKt.ui.graphics

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaPosition2D
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaRectangle
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaSize2D

/**
 * The graphics interface.
 * @since EFL v1.22
 */
public expect interface GraphicsEntityBase : EflObjectBase {
    /**
     * Rectangular geometry that combines both position and size.
     * @since EFL v1.22
     */
    public open var geometry: EinaRectangle

    /**
     * The 2D position of a canvas object. Position is absolute in pixels, relative to the top-left corner of the
     * window, within its border decorations (application space).
     * @since EFL v1.22
     */
    public open var position: EinaPosition2D

    /**
     * The scaling factor of an object. This property is an individual scaling factor on the object (UI widget). Note
     * that this property will affect this object's part sizes. If scale is not zero then the individual scaling will
     * override any global scaling set, for the object parts. Set it back to zero to get the effects of the global
     * scaling again.
     * @since EFL v1.22
     */
    public open var scale: Double

    /**
     * The 2D size of a canvas object.
     * @since EFL v1.22
     */
    public open var size: EinaSize2D

    /**
     * The visibility of a canvas object. All canvas objects will become visible by default just before render. This
     * means that it is not required to set the [visible] property after creating an object, unless you want to create
     * it without showing it. Note that this behavior is new since EFL v1.21, and only applies to canvas objects
     * created with the EO API (i.e. not the legacy C-only API). Other types of graphics objects may or may not be
     * visible by default.
     *
     * Note that many other parameters can prevent a visible object from actually being **visible** on screen. For
     * instance if its color is fully transparent, or its parent is hidden, or it is clipped out, etc...
     * @since EFL v1.22
     */
    public open var visible: Boolean
}
