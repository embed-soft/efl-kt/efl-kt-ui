package io.gitlab.embedSoft.eflKt.ui.graphics

/** Contains RGBA color metadata. */
public data class RgbaColor(public val red: Int, public val green: Int, public val blue: Int, public val alpha: Int)
