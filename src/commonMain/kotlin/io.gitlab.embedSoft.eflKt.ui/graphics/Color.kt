package io.gitlab.embedSoft.eflKt.ui.graphics

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/**
 * Provides basic color support.
 * @since EFL v1.22
 */
public expect interface Color : EflObjectBase {
    /**
     * Hexadecimal color code of given Evas object (**#RRGGBBAA**).
     * @since EFL v1.22
     */
    public open var colorCode: String

    /**
     * Gets the main color's RGB component (and alpha channel) values, which range from *0* to *255*. For the alpha
     * channel, which defines the object's transparency level, *0* means totally transparent while *255* means opaque.
     * These color values are pre-multiplied by the alpha value. Usually you'll use this function for text and
     * rectangle objects, where the main color is the only color.
     * @return The RGBA color metadata.
     * @since EFL v1.22
     */
    public open fun getColor(): RgbaColor

    /**
     * Sets the main color's RGB component (and alpha channel) values, which range from *0* to *255*. For the [alpha]
     * channel, which defines the object's transparency level, *0* means totally transparent, while *255* means opaque.
     * These color values are pre-multiplied by the alpha value. Usually you'll use this function for text and
     * rectangle objects, where the main color is the only color.
     * @param red The red value to use.
     * @param green The green value to use.
     * @param blue The blue value to use.
     * @param alpha The alpha value to use.
     * @since EFL v1.22
     */
    public open fun setColor(red: Int, green: Int, blue: Int, alpha: Int)
}
