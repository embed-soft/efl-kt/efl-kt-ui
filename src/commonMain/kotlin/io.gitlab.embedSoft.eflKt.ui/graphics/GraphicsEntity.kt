package io.gitlab.embedSoft.eflKt.ui.graphics

/** Default implementation of [GraphicsEntity]. */
public expect class GraphicsEntity : GraphicsEntityBase
