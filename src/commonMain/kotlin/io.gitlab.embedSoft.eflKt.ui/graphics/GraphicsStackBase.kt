package io.gitlab.embedSoft.eflKt.ui.graphics

import io.gitlab.embedSoft.eflKt.core.EflObjectBase

/** The graphics stack interface. */
public expect interface GraphicsStackBase : EflObjectBase {
    public open val above: GraphicsStackBase

    public open val below: GraphicsStackBase

    /**
     * The layer of its canvas that the given object will be part of. If you don't use this property then you'll be
     * dealing with a unique layer of objects (the default one). Additional layers are handy when you don't want a set
     * of objects to interfere with another set with regard to stacking. Two layers are completely disjoint in that
     * matter.
     * @since EFL v1.22
     */
    public open var layer: Short

    /**
     * Lowers the object to the bottom of its layer. Object will then be the lowest one in the layer it belongs to.
     * Objects on other layers won't get touched.
     * @see stackAbove
     * @see stackBelow
     * @see raiseToTop
     * @since EFL v1.22
     */
    public open fun lowerToBottom()

    /**
     * Raise the object to the top of its layer. Object will then be the highest one in the layer it belongs to.
     * Objects on other layers won't get touched.
     * @since EFL v1.22
     * @see stackAbove
     * @see stackBelow
     * @see lowerToBottom
     */
    public open fun raiseToTop()

    /**
     * Stack the object immediately above. Objects in a given canvas are stacked in the order they're added. This means
     * that if they overlap the highest ones will cover the lowest ones, in that order. This function is a way to
     * change the stacking order for the objects. Its intended to be used with objects belonging to the same layer in a
     * given canvas, otherwise it will fail (and accomplish nothing).
     *
     * If you have smart objects on your canvas and the object is a member of one of them, then [above] must also be a
     * member of the same smart object. Similarly if the object is not a member of a smart object, [above] must not be
     * either.
     * @param above The object above which to stack.
     * @see layer
     * @see stackBelow
     * @since EFL v1.22
     */
    public open fun stackAbove(above: GraphicsStackBase)

    /**
     * Stack the object immediately below. Objects in a given canvas are stacked in the order they're added. This means
     * that if they overlap the highest ones will cover the lowest ones, in that order. This function is a way to
     * change the stacking order for the objects. Its intended to be used with objects belonging to the same layer in a
     * given canvas, otherwise it will fail (and accomplish nothing).
     *
     * If you have smart objects on your canvas and the object is a member of one of them then [below] must also be a
     * member of the same smart object. Similarly if the object isn't a member of a smart object, [below] must not be
     * either.
     * @param below The object below which to stack.
     * @see layer
     * @see stackAbove
     * @since EFL v1.22
     */
    public open fun stackBelow(below: GraphicsStackBase)
}
