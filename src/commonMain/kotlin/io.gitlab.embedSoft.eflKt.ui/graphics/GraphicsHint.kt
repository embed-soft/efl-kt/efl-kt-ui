package io.gitlab.embedSoft.eflKt.ui.graphics

import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.dataType.graphics.EinaSize2D

/**
 * Provides graphics hints for an object.
 * @since EFL v1.22
 */
public expect interface GraphicsHint : EflObjectBase {
    /**
     * Hints for an object's alignment. These are hints on how to align an object inside the boundaries of a
     * container/manager. Accepted values are in the *0.0* to *1.0* range. For the horizontal component, *0.0* means
     * the start of the axis in the direction that the current language reads, *1.0* means the end of the axis. With
     * the vertical component, *0.0* to the top, *1.0* means to the bottom. This property is a Pair containing the
     * following:
     * 1. xPos - X position ranging from *0.0* to *1.0*, where *0.0* is at the start of the horizontal axis and *1.0*
     * is at the end.
     * 2. yPos - Y position ranging from *0.0* to *1.0*, where *0.0* is at the start of the vertical axis and *1.0* is
     * at the end.
     *
     * This is not a size enforcement in any way, it's just a hint that should be used whenever appropriate.
     * @since EFL v1.22
     */
    public open var hintAlign: Pair<Double, Double>

    /**
     * Defines the aspect ratio to respect when scaling this object. The aspect ratio is defined as the width / height
     * ratio of the object. Depending on the object and its container, this hint may or may not be fully respected. If
     * any of the given aspect ratio terms are *0* then the object's container will ignore the aspect, and scale this
     * object to occupy the whole available area, for any given policy.
     *
     * This property is a Pair containing the following:
     * 1. mode - Mode of interpretation.
     * 2. size - Base size to use for aspecting.
     * @since EFL v1.22
     */
    public open var hintAspect: Pair<UInt, EinaSize2D>

    /**
     * Hints for an object's fill property that used to specify **justify** or **fill** by some users. This property
     * specifies whether to fill the space inside the boundaries of a container/manager. Maximum hints should be
     * enforced with higher priority, if they are set. Also any [hintMargin] set on objects should add up to the object
     * space on the final scene composition. This is not a size enforcement in any way, it's just a hint that should be
     * used whenever appropriate.
     *
     * This property is a Pair containing the following:
     * 1. fillX - A value of *true* if to fill the object space, *false* otherwise to use as horizontal fill hint.
     * 2. fillY - A value of *true* if to fill the object space, *false* otherwise to use as vertical fill hint.
     * @since EFL v1.22
     */
    public open var hintFill: Pair<Boolean, Boolean>

    /**
     * Hints for an object's margin or padding space. This is not a size enforcement in any way, it's just a hint that
     * should be used whenever appropriate. The object container is in charge of fetching this property, and placing
     * the object accordingly.
     * @since EFL v1.22
     */
    public open var hintMargin: GraphicsHintMargin

    /** Maximum size (hint) in pixels. */
    public open val hintSizeCombinedMax: EinaSize2D

    /** Minimum size (hint) in pixels. */
    public open val hintSizeCombinedMin: EinaSize2D

    /**
     * Hints on the object's maximum size. This is not a size enforcement in any way, it's just a hint that should be
     * used whenever appropriate. The object container is in charge of fetching this property, and placing the object
     * accordingly. A value of *-1* will be treated as unset hint components, when queried by managers.
     * @since EFL v1.22
     */
    public open var hintSizeMax: EinaSize2D

    /**
     * Hints on the object's minimum size. This is not a size enforcement in any way, it's just a hint that should be
     * used whenever appropriate. The object container is in charge of fetching this property, and placing the object
     * accordingly. A value of *0* will be treated as unset hint components, when queried by managers.
     * @since EFL v1.22
     */
    public open var hintSizeMin: EinaSize2D

    /**
     * Hints for an object's weight. This is a hint on how a container object should resize a given child within its
     * area. Containers may adhere to the simpler logic of just expanding the child object's dimensions to fit its own
     * ,or the complete one of taking each child's weight hint as real weights to how much of its size to allocate for
     * them in each axis. A container is supposed to after normalizing the weights of its children (with weight hints),
     * distribute the space it has to layout them by those factors. Most weighted children get larger in this process
     * than the least ones.
     *
     * Accepted values are zero or positive values. Some containers might use this hint as a boolean, but some others
     * might consider it as a proportion. See the documentation of each container. This property is a Pair containing
     * the following:
     * 1. x - Non-negative double value to use as horizontal weight hint.
     * 2. y - Non-negative double value to use as vertical weight hint.
     */
    public open var hintWeight: Pair<Double, Double>
}
